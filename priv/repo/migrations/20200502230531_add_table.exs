defmodule ApiStore.Repo.Migrations.AddTable do
  use Ecto.Migration

  def change do
    create table(:starred_repos) do
      add :user, :string
      add :id_repo, :integer
      add :name, :string
      add :description, :string
      add :url, :string
      add :language, :string
      add :tags, :string
      timestamps()
    end
  end
end
