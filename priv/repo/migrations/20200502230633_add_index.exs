defmodule ApiStore.Repo.Migrations.AddIndex do
  use Ecto.Migration

  def change do
    create index(:starred_repos, :user)
  end
end
