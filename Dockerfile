# ./Dockerfile
# base image Elixir to start with
FROM elixir:1.12

RUN apt-get update && apt-get install -y apt-utils && apt-get install --yes postgresql-client

# install hex package manager
RUN mix local.hex --force
RUN mix local.rebar --force

# create our app folder and copy our code in it
RUN mkdir /app
COPY . /app
WORKDIR /app
EXPOSE 4100

# install dependencies
RUN . ./.env
RUN mix deps.clean --all --unlock
RUN mix deps.get
RUN mix deps.compile

# run api gateway server
CMD ["./run.sh"]
