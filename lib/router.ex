defmodule ApiSearch.Router do
  @moduledoc """
    Documentation about routes on API Search Project
    Routes =>
      get /search => Search all starred repos for all users on local database

      get /search/:user => Search starred repos for only one user

      get /search/tag => Search all tagged repos for all users on local database if exists.

      get /search/tag/:tag => Search for :tag expression  for tagged repos if exists.

      post /update => Update local database with tags with pattern: "tag1,tag2,tag3..."
  """
  use Plug.Router
  require ApiSearch

  plug(:match)
  plug Plug.Parsers, parsers: [:urlencoded, :multipart, :json],
                     pass:  ["*/*"],
                     json_decoder: Poison
  plug(:dispatch)

  get "/search/:user" do
    %{status_code: status_code, body: body} = ApiSearch.get_starred("#{user}")
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(status_code, body)
  end

  get "/search" do
    %{status_code: status_code, body: body} = ApiSearch.get_starred("*")
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(status_code, body)
  end

  get "/search/tag" do
    %{status_code: status_code, body: body} = ApiSearch.get_starred_by_tag("*")
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(status_code, body)
  end

  get "/search/tag/:tag" do
    %{status_code: status_code, body: body} = ApiSearch.get_starred_by_tag("#{tag}")
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(status_code, body)
  end

  post "/update" do
    p = conn.params
    %{body: body, status_code: status_code} = ApiSearch.update_tags(p["user"],p["id_repo"],p["tags"])
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(status_code, body)
  end
end
