defmodule ApiStore do

  @moduledoc """
  Documentation for `ApiStore`.
  That module is responsible for low level database
  communication via Ecto.Query with CRU operations.
  """

  import Ecto.Query, only: [from: 2]
  require ApiStore.Repo
  use Application

  def get_data(user) do
    query = case user do
            "*" -> from r in "starred_repos",
                    select: %{id_repo: r.id_repo, name: r.name, description: r.description,
                              url: r.url, language: r.language, user: r.user, tags: r.tags},
                    order_by: r.user

            _   -> from r in "starred_repos",
                    where: [user: ^user],
                    select: %{id_repo: r.id_repo, name: r.name, description: r.description,
                              url: r.url, language: r.language, user: r.user, tags: r.tags},
                    order_by: r.user
            end
    ApiStore.Repo.all(query)
  end

  def get_data_tag(tag) do
    query = case tag do
      "*" -> from r in "starred_repos",
              where: not is_nil(r.tags),
              select: %{id_repo: r.id_repo, name: r.name, description: r.description,
                        url: r.url, language: r.language, user: r.user, tags: r.tags},
              order_by: r.user

      _   -> tag = "%" <> tag <> "%"
             from r in "starred_repos",
              where: like(r.tags, ^tag),
              select: %{id_repo: r.id_repo, name: r.name, description: r.description,
                        url: r.url, language: r.language, user: r.user, tags: r.tags},
              order_by: r.user
      end
    ApiStore.Repo.all(query)
  end

  def insert_data(data) do
    ApiStore.Repo.insert!(data)
  end

  def update_data(user,id_repo,tags) do
    starred = ApiStore.Starred_Repos
              |> Ecto.Query.where(user: ^user)
              |> Ecto.Query.where(id_repo: ^id_repo)
              |> ApiStore.Repo.one
    changeset = ApiStore.Starred_Repos.changeset(starred, %{tags: tags})
    case ApiStore.Repo.update(changeset) do
      {:ok, starred} -> %{body: Poison.encode!(%{message: msgs(:success_update_tags)}), status_code: 200}
      {:error, changeset} -> %{body: Poison.encode!(%{message: changeset.errors}), status_code: 400}
    end
  end

  defp msgs(m), do: Application.get_env(:api_search, m)
end
