defmodule ApiSearch.Endpoint do
  use Plug.Router
  use Application

  plug(:match)

  plug(Plug.Parsers,
    parsers: [:json],
    pass: ["application/json"],
    json_decoder: Poison
  )

  plug(:dispatch)

  forward("/api", to: ApiSearch.Router)

  match _ do
    send_resp(conn, 404, msgs(:page_not_found))
  end

  def child_spec(opts) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [opts]}
    }
  end

  def start_link(_opts),do: Plug.Cowboy.http(__MODULE__, [])

  defp msgs(m), do: Application.get_env(:api_search, m)
end
