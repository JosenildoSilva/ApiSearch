defmodule ApiSearch do

  @moduledoc """
  Documentation for `ApiSearch`.
  The main purpose for that module is for get all data
  about starred repositories for one Github User.
  """

  require ApiStore
  require Logger
  use Application

  @doc """
  Method: get_starred - Get all starred repos for one user
  Input Parameter(s):  user on GitHub, type: string.
  Output Data: JSON_ARRAY with starred repos data
  """
  def get_starred(user) do
    starred = ApiStore.get_data(user)
    case starred do
      [] ->
        url = "https://api.github.com/users/#{user}/starred"
        headers = ["Accept": "application/vnd.github.v3.star+json"]
        options = [ssl: [{:versions, [:"tlsv1.2"]}]]
        data_result = HTTPoison.get(url, headers, options)
        case data_result do
          {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
            case body do
              "[]" ->
                %{body: encode_response(%{message: msgs(:not_found_starred)}), status_code: 400}

              _ ->
                result = Enum.map(decode_response(body), fn data ->
                  save_data(data,user)
                  format_response(data,user)
                end
                )
                %{body: encode_response(result), status_code: 200}
            end

          {:ok, %HTTPoison.Response{body: message, headers: headers, request: request}} ->
            %{body: encode_response(%{message: msgs(:user_not_found)}), status_code: 404}

          {:error, %HTTPoison.Error{reason: reason}} ->
            %{body: reason, status_code: 404}
        end

      _ ->
        %{body: encode_response(starred), status_code: 200}
    end
  end

  @doc """
  Method: get_starred_by_tag - Get starred repos by one tag
  Input Parameter(s):
        - tag this is name for one tag putted for the user, type: string

  Output Data: JSON_ARRAY with starred repos data by tag.
  """
  def get_starred_by_tag(tag) do
    result = ApiStore.get_data_tag(tag)
    case result do
      [] -> %{body: encode_response(%{message: msgs(:no_data_for_tag)}), status_code: 404}
      _  -> %{body: encode_response(result), status_code: 200}
    end
  end

  @doc """
  Method: update_tags - Update tags on one starred repo
  Input Parameter(s):
        - user name from GitHub, type: string
        - id_repo for one starred repo, type: integer
        - tags for one starred repo, type: string, format: csv line with separator symbol(,)
  Output Data: Success or Error message.
  """
  def update_tags(user,id_repo,tags) do
    ApiStore.update_data(user,id_repo,tags)
  end

  defp encode_response(body) do
    {:ok, body} = Poison.encode(body)
    body
  end

  defp decode_response(body) do
    {:ok, body} = Poison.decode(body)
    body
  end

  defp format_response(data, user) do
    new_data = %{
      id_repo: data["repo"]["id"],
      name: data["repo"]["name"],
      description: data["repo"]["description"],
      url: data["repo"]["html_url"],
      language: data["repo"]["language"],
      user: user,
      tags: ""
    }
    new_data
  end

  defp save_data(data,user) do
    new_data = %ApiStore.Starred_Repos{
      :id_repo => data["repo"]["id"],
      :name => data["repo"]["name"],
      :description => data["repo"]["description"],
      :url => data["repo"]["html_url"],
      :language => data["repo"]["language"],
      :user => user,
      :tags => ""
    }
    ApiStore.insert_data(new_data)
  end

  defp msgs(m),do: Application.get_env(:api_search, m)
end
