defmodule ApiStore.Starred_Repos do

  @moduledoc """
  Documentation for `ApiStore.Starred_Repos`.
  That module is responsible for low level database Schema
  that represent data on starred_repos table on database(Example: PostgreSQL, MySQL).
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "starred_repos" do
    field :user, :string
    field :id_repo, :integer
    field :name, :string
    field :description, :string
    field :url, :string
    field :language, :string
    field :tags, :string
    timestamps()
  end

  @doc """
  Method: changeset is responsible for stablish some rules for changes on data.
          The tags field are unique and required on updates if included or excluded one tag.
  Input params:
    - tbl is the name for one table schema.
    - params represent data sent for update.
  Output:
    - The changeset object with all data for update
  """
  def changeset(tbl, params \\ %{}) do
    tbl
    |> cast(params, [:tags])
    |> validate_required([:tags])
    |> unique_constraint(:tags)
  end
end
