defmodule ApiStore.Repo do
  use Ecto.Repo,
    otp_app: :api_search,
    adapter: Ecto.Adapters.Postgres
end
