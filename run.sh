#!/bin/bash

. ./.env
USER="$POSTGRES_USER"
PWD="$POSTGRES_PASSWORD"
HOST="$POSTGRES_HOST"
DB="$POSTGRES_DB"
CMD1=`PGPASSWORD=$PWD psql -h $HOST -U $USER echo  -c '\q' 2>/dev/null`
CMD2=`PGPASSWORD=$PWD psql -h $HOST -U $USER -lqt | cut -d \| -f 1 | grep $DB`

set -e

# Wait for Postgres to become available.
until $CMD1; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 5
done

if test -z $CMD2; then
mix ecto.create
mix ecto.migrate
mix run --no-halt
else 
mix run --no-halt
fi

printf "\nServer started with success...\n"