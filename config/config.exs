# This file is responsible for configuring your
# application and their dependencies with the
# help of the Config module.
import Config


# PROD CONFIG PARAMETERS POSTGRESQL
config :api_search, ApiStore.Repo,
username: System.get_env("POSTGRES_USER"),
password: System.get_env("POSTGRES_PASSWORD"),
database: System.get_env("POSTGRES_DB"),
hostname: System.get_env("POSTGRES_HOST")

# DEV CONFIG PARAMETERS POSTGRESQL
# config :api_search, ApiStore.Repo,
# username: "postgres",
# password: "_Welcome1",
# database: "api_store_repo",
# hostname: "localhost"

config :api_search, ecto_repos: [ApiStore.Repo]

config :api_search, cowboy_port: 4100,
  debug_errors: true,
  code_reloader: true,
  check_origin: false

# Sample configuration:
config :logger, :console,
  level: :info,
  format: "$date $time [$level] $metadata$message\n",
  metadata: [:user_id]

config :postgrex, :json_library, Poison

config :api_search, success_update_tags: "Record for starred repo was updated",
 user_not_found: "User not found on github",
 page_not_found: "Requested page not found",
 not_found_starred: "The user not have starred repos",
 no_data_for_tag: "Not exist any tagged repo(s)"
