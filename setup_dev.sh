#!/bin/sh
. ./.env
mix local.hex --force
mix local.rebar --force
mix deps.clean --all --unlock
mix deps.get
mix ecto.create
mix ecto.migrate
mix deps.compile