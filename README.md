# API Search Starred Repos

**The main purpose is search starred repos**

## Project Details

The project is based on Elixir.Plug pattern and follow the specifications for implement one thin REST API with JSON without need one complete web framework, not needed in that situation.

The file config/config.exs contains all general configurations and resources for the complete execution for that app.

The project is containerized with docker and docker-compose and contains too all needed Ecto libraries for connect and use PostgreSQL latest version. For simplicity I have used only one config file with two sessions for configure database parameters for DEV or PROD. You need only find the comment in each session for each environment and comment the session that you not need and uncomment the session for your correct environment.

The project was developed with PostgreSQL in mind but you can change the database configurations for MYSQL, for example, on your workstations.

For all steps here you need have Elixir latest release installed on your machine and need follow the steps [available in Elixir Install](https://elixir-lang.org/install.html) for your platform(Windows, Linux, MacOSX et).

The instructions here are applied to LINUX or MACOSX platforms. For Windows need adapt scripts for execution or use the platform for Ubuntu on Windows 10 for example.

## Using Local DEV or PROD(Docker) Environment

For work on DEV/PROD environment you need configure parameters on .env local file as below:

```
# PostgreSQL
# export POSTGRES_HOST=localhost # Local DEV
export POSTGRES_HOST=dbserver # Docker PROD
export POSTGRES_PORT=5432
export POSTGRES_DB=api_store_db
export POSTGRES_USER=usertest
export POSTGRES_PASSWORD=welcome1
export POSTGRES_POOL=100
```

You can change each parameter for the correct configuration for your database.

For execute tests you need follow that steps:

1) Clone the project from github:
   
   git clone https://github.com...

2) Execute the following commands on root of project on terminal for install dependencies and create database and tables:
   
   /> chmod +x setup_dev.sh

   /> ./setup_dev.sh

3) For execute Plug.Test tests execute on root of project:
   
   /> mix test

4) For local app execution on your environment you can execute on root of your project:

   /> mix run --no-halt

## Generating and Executing PROD Package

For generate and execute the PROD package follow the steps:

1) You need install [docker](https://docs.docker.com/get-docker/) and [docker-compose](https://docs.docker.com/compose/install/)

2) If your PostgreSQL is started on hosting machine you need stop from your workstation, on Ubuntu for example execute:
      
   /> sudo service postgresql stop

3) From root on your project execute:
      
   /> docker-compose build && docker-compose up

4) After some time the system is ready for you execute tests on PROD Package via Browser execute, for example:   
   
   http://localhost:4100/api/search/mike


## API documentation

On folder doc on root of project I have generated all documentation about the API.
If you open the index.html on browser you read all specific material about the API(Routes, modules, functions, et).