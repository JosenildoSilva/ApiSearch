defmodule ApiSearch.MixProject do
  use Mix.Project

  def project do
    [
      app: :api_search,
      version: "0.1.0",
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      name: "ApiSearch Starred Repos",
      source_url: "https://github.com/USER/PROJECT",
      homepage_url: "http://YOUR_PROJECT_HOMEPAGE",
      docs: [
        logo: "priv/assets/images/logo.png",
        extras: ["README.md"]
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger,:ecto_sql,:postgrex, :httpoison, :poison],
      mod: {ApiSearch.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.31", only: :dev, runtime: false},
      {:httpoison, "~> 2.2"},
      {:poison, "~> 5.0"},
      {:plug, "~> 1.15"},
      {:plug_cowboy, "~> 2.6"},
      {:cowboy, "~> 2.10"},
      {:ecto_sql, "~> 3.11"},
      {:postgrex, "~> 0.17"}
    ]
  end
end
