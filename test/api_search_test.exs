defmodule ApiSearchTest do
  use ExUnit.Case, async: true
  use Plug.Test
  import Ecto.Query, only: [from: 2]
  require ApiStore.Repo
  doctest ApiSearch

  @opts ApiSearch.Router.init([])

  test "get result from search without user if local database is empty" do
    query = from "starred_repos", select: [:user]
    result = ApiStore.Repo.all(query,[])
    # Create a test connection
    case result do
      [] ->
        conn =
          :get
          |> conn("/search", "")
          |> ApiSearch.Router.call(@opts)

        # Assert the response and status
        assert conn.state == :sent
        assert conn.status == 404
        assert conn.resp_body == "{\"message\":\"User not found on github\"}"
      _ -> nil
    end
  end

  test "get result from user without starred repos" do
    # Create a test connection
    conn =
      :get
      |> conn("/search/tanujam", "")
      |> ApiSearch.Router.call(@opts)

    # Assert the response and status
    assert conn.state == :sent
    assert conn.status == 400
    assert conn.resp_body == "{\"message\":\"The user not have starred repos\"}"
  end

  test "get result from user that not exists on github" do
    # Create a test connection
    conn =
      :get
      |> conn("/search/tanijam", "")
      |> ApiSearch.Router.call(@opts)

    # Assert the response and status
    assert conn.state == :sent
    assert conn.status == 404
    assert conn.resp_body == "{\"message\":\"User not found on github\"}"
  end

  test "get data from user starred repos" do
    # Create a test connection
    conn =
      :get
      |> conn("/search/mike", "")
      |> ApiSearch.Router.call(@opts)

    # Assert the response and status
    assert conn.state == :sent
    assert conn.status == 200
    assert conn.resp_body != []
  end

  test "update data for starred repos tags" do
    # Create a test connection
    conn =
      :post
      |> conn("/update", %{user: "mike", id_repo: 6391425, tags: "python,rest"})
      |> ApiSearch.Router.call(@opts)

    # Assert the response and status
    assert conn.state == :sent
    assert conn.status == 200
    assert conn.resp_body == "{\"message\":\"Record for ​​starred repo was updated​\"}"
  end

  test "get data from user starred repos by tag" do
    # Create a test connection
    conn =
      :get
      |> conn("/search/tag/python", "")
      |> ApiSearch.Router.call(@opts)

    # Assert the response and status
    assert conn.state == :sent
    assert conn.status == 200
    assert conn.resp_body != []
  end

  test "get data from user starred repos without tag param" do
    # Create a test connection
    conn =
      :get
      |> conn("/search/tag", "")
      |> ApiSearch.Router.call(@opts)

    # Assert the response and status
    assert conn.state == :sent
    assert conn.status == 200
    assert conn.resp_body != []
  end
end
